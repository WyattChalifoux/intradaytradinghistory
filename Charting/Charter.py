import matplotlib.pyplot as plt
import csv
import datetime
import matplotlib.dates as mdates
import time

def readTimesAndClosesFromCSV(filepath):
    timesAndCloses = []
    csv_reader = csv.reader(open(filepath), delimiter=',')
    line_count = 0
    next(csv_reader)
    testBreakOut = 0
    for row in csv_reader:
        testBreakOut += 1
        if testBreakOut > 20000000:
            return timesAndCloses
        if(len(row) >= 4):
            if row[0] != "time":
                timesAndCloses.append([row[0], row[4]])
    return timesAndCloses

def hashByDays(timesAndClosesList):
    dayDictionary = dict()
    for timeAndClose in timesAndClosesList:
        day = timeAndClose[0][0:11].strip()
        time = timeAndClose[0][11:].strip()
        if day in dayDictionary:
            dayDictionary[day].insert(0, [time,  timeAndClose[1]])
        else:
            dayDictionary[day] = [[time,  timeAndClose[1]]]
    return dayDictionary

def extractEarliestAndLatestSharedTime(dayDictionary):
    earliestTimeList = []
    latestTimeList = []
    for day in dayDictionary:
        earliestTimeList.append(datetime.datetime.strptime(dayDictionary[day][0][0], "%H:%M:%S"))
        latestTimeList.append(datetime.datetime.strptime(dayDictionary[day][-1][0], "%H:%M:%S"))
    #Find the latest time of the earliest times, and the earliest time of the latest times
    earliestTimeList.sort()
    earliestTimeList.reverse()
    latestTimeList.sort()
    return sorted([earliestTimeList[0], latestTimeList[0]])

def convertDayDictionaryToDateTimeFormat(dayDictionary):
    for day in dayDictionary:
        for timeAndClose in dayDictionary[day]:
            timeAndClose[0] = datetime.datetime.strptime(timeAndClose[0], "%H:%M:%S")

def removeOutlierTimes(dayDictionary, earliestTime, latestTime):
    for day in dayDictionary:
        dayDictionary[day] = [timeAndClose for timeAndClose in dayDictionary[day]
                              if not outlier(timeAndClose, earliestTime, latestTime)]

def outlier(timeAndClose, earliestTime, latestTime):
    if timeAndClose[0] < earliestTime or timeAndClose[0] > latestTime:
        return True
    return False

def fillInMissingTimes(dayPercentageDictionary, plotTimeRange):
    for day in dayPercentageDictionary:
        timesAndPercents = dayPercentageDictionary[day]
        timesAndPercentsDictionary = dict()
        for time in plotTimeRange:
            timesAndPercentsDictionary[time] = [time, 0]
        for timeAndPercent in timesAndPercents:
            timesAndPercentsDictionary[timeAndPercent[0]][1] = timeAndPercent[1]
        dayPercentageDictionary[day] = list(timesAndPercentsDictionary.values())


def getDayPercentageDictionary(dayDictionary):
    dayPercentageDictionary = dict()
    for day in dayDictionary:
        timesAndCloses = dayDictionary[day]
        initialPrice = float(timesAndCloses[0][1])
        timesAndPercents = []
        for timeAndClose in timesAndCloses:
            percentChangeFromInitial = (float(timeAndClose[1])/initialPrice - 1)*100
            timesAndPercents.append([timeAndClose[0], percentChangeFromInitial])
        dayPercentageDictionary[day] = timesAndPercents

    return dayPercentageDictionary

def generatePlotTimeRangeFromSharedTimes(earliestTime, latestTime):
    currentTime = earliestTime
    plotTimeRange = []
    while currentTime <= latestTime:
        plotTimeRange.append(currentTime)
        currentTime = currentTime + datetime.timedelta(0, 0, 0, 0, 1)
    return plotTimeRange

def calculateAveragePercentChangeAtTime(dayPercentageDictionary, plotTimeRange):
    averageData = dict()
    for timeIndex in range(len(plotTimeRange)):
        averagePercentChangeAtTime = 0
        for day in dayPercentageDictionary:
            averagePercentChangeAtTime += dayPercentageDictionary[day][timeIndex][1]
        averagePercentChangeAtTime = averagePercentChangeAtTime / len(dayPercentageDictionary.keys())
        averageData[plotTimeRange[timeIndex]] = averagePercentChangeAtTime
    return averageData

def highlightDay(highlightedDay, ax, plotTimeRange, dayPercentageDictionary, color, linewidth):
    ax.plot(plotTimeRange,
            [timeAndPercent[1] for timeAndPercent in dayPercentageDictionary[highlightedDay]],
            alpha=1, color=color, linewidth=linewidth)  # Plot some data on the axes.

def findMostSimilarDays(sourceDay, dayPercentageDictionary):
    dayLeastSquareSimilarity = dict()
    for day in dayPercentageDictionary:
        sumOfSquares = 0
        for timeIndex in range(len(dayPercentageDictionary[sourceDay])):
            difference = dayPercentageDictionary[sourceDay][timeIndex][1] - dayPercentageDictionary[day][timeIndex][1]
            sumOfSquares += difference**2
        dayLeastSquareSimilarity[day] = sumOfSquares
    return dict(sorted(dayLeastSquareSimilarity.items(), key=lambda item: item[1]))

def highlightMostSimilarDays(dayLeastSquareSimilarity, ax, plotTimeRange, dayPercentageDictionary, numberOfResults):
    resultsShown = 0
    mostSimilarDay = list(dayLeastSquareSimilarity.values())[1]
    for day in reversed(list(dayLeastSquareSimilarity.keys())[1:numberOfResults]):
        relativeSimilarity = mostSimilarDay/dayLeastSquareSimilarity[day]
        highlightDay(day, ax, plotTimeRange, dayPercentageDictionary, (relativeSimilarity, 0, 1), relativeSimilarity*2)
        resultsShown += 1

def getNextTradingDay(day, dayPercentageDictionary):
    date = datetime.date.fromisoformat(day)
    try:
        date = date + datetime.timedelta(days=1)
        while date.isoformat() not in dayPercentageDictionary:
            date = date + datetime.timedelta(days=1)
        return date.isoformat()
    except:
        print("Date error encountered: Using previous day")
        return day

def highlightDaysAfterMostSimilarDays(dayLeastSquareSimilarity, ax, plotTimeRange, dayPercentageDictionary, numberOfResults):
    resultsShown = 0
    mostSimilarDay = list(dayLeastSquareSimilarity.values())[1]
    for day in reversed(list(dayLeastSquareSimilarity.keys())[1:numberOfResults]):
        relativeSimilarity = mostSimilarDay/dayLeastSquareSimilarity[day]
        highlightDay(getNextTradingDay(day, dayPercentageDictionary), ax, plotTimeRange, dayPercentageDictionary, (0, relativeSimilarity, 1), relativeSimilarity*2)
        resultsShown += 1

def chartDays(filepath):
    print("Reading file")
    timesAndCloses = readTimesAndClosesFromCSV(filepath)
    timesAndCloses.reverse()
    print("Making day dictionary")
    dayDictionary = hashByDays(timesAndCloses)
    print("Finding time bounds")
    earliestAndLatestSharedTimes = extractEarliestAndLatestSharedTime(dayDictionary)
    print("Converting to datetime format")
    convertDayDictionaryToDateTimeFormat(dayDictionary)
    print("Removing outliers")
    removeOutlierTimes(dayDictionary, earliestAndLatestSharedTimes[0], earliestAndLatestSharedTimes[1])
    print("Extracting time range")
    plotTimeRange = generatePlotTimeRangeFromSharedTimes(earliestAndLatestSharedTimes[0], earliestAndLatestSharedTimes[1])
    print("Creating percentage dictionary")
    dayPercentageDictionary = getDayPercentageDictionary(dayDictionary)
    print("Filling in missing times")
    fillInMissingTimes(dayPercentageDictionary, plotTimeRange)
    fig, ax = plt.subplots()  # Create a figure containing a single axes.
    # for i in range(0, 300):
    #     ax.plot(range(600),
    #             [random.randrange(1, 50)*i for i in range(600)],
    #             alpha=0.05,  color='b', linewidth='0.1')  # Plot some data on the axes.
    print("Beginning plotting")
    for day in dayPercentageDictionary:
        ax.plot(plotTimeRange,
                [timeAndPercent[1] for timeAndPercent in dayPercentageDictionary[day]],
                alpha=0.01, color='b', linewidth='0.5')  # Plot some data on the axes.


    selectedDay = '2021-03-11'
    dayLeastSquareSimilarity = findMostSimilarDays(selectedDay, dayPercentageDictionary)
    #highlightMostSimilarDays(dayLeastSquareSimilarity, ax, plotTimeRange, dayPercentageDictionary, 20)
    highlightDaysAfterMostSimilarDays(dayLeastSquareSimilarity, ax, plotTimeRange, dayPercentageDictionary, 20)
    highlightDay(selectedDay, ax, plotTimeRange, dayPercentageDictionary, (0, .9, 0), 2)
    #averageData = calculateAveragePercentChangeAtTime(dayPercentageDictionary, plotTimeRange)
    # ax.plot(plotTimeRange,
    #         list(averageData.values()),
    #         alpha=1, color='r', linewidth='2')  # Plot some data on the axes.
    ax.xaxis.set_major_locator(mdates.HourLocator(interval=1))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    ax.set_ylabel('% Change')
    ax.set_title('SPY Intraday Movement\n(' + str(len(dayPercentageDictionary.keys())) + " days)")
    plt.show()