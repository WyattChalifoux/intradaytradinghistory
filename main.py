from AlphaVantageConnector.alphaVantageConnector import writeIntradayExtended
from Charting.Charter import chartDays

filenameBase = "spyHistory"
writeIntradayExtended(filenameBase, "SPY")
#chartDays("./ResponseFiles/" + filenameBase + "response.csv")
chartDays("./ResponseFiles/SPY_1min.txt")