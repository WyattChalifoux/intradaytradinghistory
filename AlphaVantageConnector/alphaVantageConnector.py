# Import the library
import csv
import requests
import time



def writeIntradayExtended(filename, symbolToRetrieve):
    with open('apiKey.txt', 'r') as file:
        apiKey = file.read()

    concatenatedContent = ""
    for year in range(1, 2):
        for month in range(1, 12):
            print("Requesting Year " + str(year) + ", month " + str(month))
            response = requests.get(
                "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY_EXTENDED&symbol=" +
                symbolToRetrieve + "&interval=1min&slice=year" + str(year) + "month" + str(month) + "&apikey=" + apiKey)
            concatenatedContent += response.content.decode("utf-8")
            # API has a 5 request per minute limit
            print("Waiting")
            time.sleep(14)
            print("Resuming")


    response_file = open("./ResponseFiles/" + filename + "response.csv", "w")
    response_file.write(concatenatedContent)
    response_file.close()
